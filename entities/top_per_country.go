package entities

// TopPerCountry represents an entry in the API resultset.
type TopPerCountry struct {
	Country  string              `json:"country"`
	Access   string              `json:"access"`
	Year     string              `json:"year"`
	Month    string              `json:"month"`
	Day      string              `json:"day"`
	Articles []PerCountryArticle `json:"articles"`
}

// PerCountryArticle represents the country data for a returned article.
type PerCountryArticle struct {
	Article   string `json:"article"`
	Project   string `json:"project"`
	ViewsCeil int    `json:"views_ceil"`
	Rank      int    `json:"rank"`
}

// TopPerCountryResponse represents the API resultset.
type TopPerCountryResponse struct {
	Items []TopPerCountry `json:"items"`
}
