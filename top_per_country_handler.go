package main

import (
	"context"
	"encoding/json"
	"net/http"
	"pageviews/logic"
	"strings"
	"time"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// TopPerCountryHandler is an HTTP handler for top-per-country API requests.
type TopPerCountryHandler struct {
	logger  *logger.Logger
	session *gocql.Session
	logic   *logic.TopPerCountryLogic
	config  *Config
}

func (s *TopPerCountryHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	var err error

	country := ctx.UserValue("country").(string)
	access := strings.ToLower(ctx.UserValue("access").(string))
	year := ctx.UserValue("year").(string)
	month := ctx.UserValue("month").(string)
	day := ctx.UserValue("day").(string)

	c, cancel := context.WithTimeout(ctx, time.Duration(s.config.ContextTimeout)*time.Millisecond)

	response := s.logic.ProcessTopPerCountryLogic(c, ctx, country, access, year, month, day, ctx.Request.URI().String(), s.logger, s.session)
	defer cancel()

	if response == nil {
		return
	}
	ctx.SetStatusCode(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Log(logger.ERROR, "Unable to marshal response object: %s", err)

		statusCode := http.StatusInternalServerError
		problemResp := aqsassist.CreateProblem(statusCode, err.Error(), string(ctx.Request.URI().RequestURI())).JSON()

		ctx.SetStatusCode(statusCode)
		ctx.SetBody(problemResp)
		return
	}
	ctx.SetBody(data)
}
