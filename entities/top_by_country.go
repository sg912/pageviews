package entities

// TopByCountry represents the top-by-country resultset.
type TopByCountry struct {
	Project   string    `json:"project"`
	Access    string    `json:"access"`
	Year      string    `json:"year"`
	Month     string    `json:"month"`
	Countries []Country `json:"countries"`
}

// CountryData represents an individual country's result in the set.
type CountryData struct {
	Country string `json:"country"`
	Views   int    `json:"views"`
	Rank    int    `json:"rank"`
}

// Country represents an individual country's data within the set.
type Country struct {
	Country   string `json:"country"`
	Views     string `json:"views"`
	Rank      int    `json:"rank"`
	ViewsCiel int    `json:"views_ciel"`
}

// TopByCountryResponse represents a container for the top-by-country resultset.
type TopByCountryResponse struct {
	Items []TopByCountry `json:"items"`
}
