package logic

import (
	"context"
	"encoding/json"
	"pageviews/entities"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

type TopLogic struct {
}

func (s *TopLogic) ProcessTopLogic(context context.Context, ctx *fasthttp.RequestCtx, project, access, year, month, day, requestURI string, requestLogger *logger.Logger, session *gocql.Session) *entities.TopResponse {
	var err error
	var articles = []entities.Article{}
	var response = entities.TopResponse{Items: make([]entities.Top, 0)}

	query := `SELECT "articlesJSON" FROM "local_group_default_T_top_pageviews".data WHERE "_domain" = 'analytics.wikimedia.org' AND project = ? AND access = ? AND year = ? AND month = ? AND day = ?`
	scanner := session.Query(query, project, access, year, month, day).WithContext(context).Iter().Scanner()
	var articlesJSON string

	for scanner.Next() {
		if err = scanner.Scan(&articlesJSON); err != nil {
			requestLogger.Log(logger.ERROR, "Query failed: %s", err)
			aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
				Ctx:    ctx,
				Detail: err.Error(),
				URL:    &requestURI,
			})
			return nil
		}
	}

	if err = json.Unmarshal([]byte(articlesJSON), &articles); err != nil {
		requestLogger.Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    &requestURI,
		})
		return nil
	}

	if err := scanner.Err(); err != nil {
		requestLogger.Log(logger.ERROR, "Error querying database: %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    &requestURI,
		})
		return nil
	}

	response.Items = append(response.Items, entities.Top{
		Project:  project,
		Access:   access,
		Year:     year,
		Month:    month,
		Day:      day,
		Articles: articles,
	})

	return &response
}
