*This page is a work in progress.*

Pageviews is a public API developed and maintained by the Wikimedia Foundation that serves analytical
data about article pageviews of Wikipedia and its sister projects. With it, you can get pageview
trends on specific articles or projects; filter by agent type or access method, and choose different
time ranges and granularities; you can also get the most viewed articles of a certain project and
timespan, and even check out the countries that visit a project the most.
