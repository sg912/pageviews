package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"pageviews/entities"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type httpErr struct {
	Title  string   `json:"title"`
	Detail []string `json:"detail"`
}

func TestPerProject(t *testing.T) {

	// Requesting a known set of results (non-leap year)
	t.Run("returns expected data for non-leap year", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/en.wikipedia/all-access/all-agents/hourly/2021010100/2021010103"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching PerProjectResponse
		// so unmarshal it to create an object of type PerProjectResponse.  Error-out if that fails.
		obj := entities.PerProjectResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.Len(t, obj.Items, 4, "Invalid number of results")
	})

	// Requesting a known set of results (hourly granularity)
	t.Run("returns expected data for hourly granularity", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/en.wikipedia/all-access/all-agents/hourly/2021010100/2021010103"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching PerProjectResponse
		// so unmarshal it to create an object of type PerProjectResponse.  Error-out if that fails.
		obj := entities.PerProjectResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.Len(t, obj.Items, 4, "Invalid number of results")
	})

	// Requesting a known set of results (daily granularity)
	t.Run("returns expected data for daily granularity", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/en.wikipedia/all-access/all-agents/daily/2021010200/2021010600"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching PerProjectResponse
		// so unmarshal it to create an object of type PerProjectResponse.  Error-out if that fails.
		obj := entities.PerProjectResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.Len(t, obj.Items, 5, "Invalid number of results")
	})

	t.Run("returns valid response when top level domain is specified in project name", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/en.wikipedia.org/all-access/all-agents/hourly/2021010100/2021010103"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching PerProjectResponse
		// so unmarshal it to create an object of type PerProjectResponse.  Error-out if that fails.
		obj := entities.PerProjectResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.Len(t, obj.Items, 4, "Invalid number of results")
	})

	t.Run("www is stripped", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/www.en.wikipedia.org/all-access/all-agents/hourly/2021010100/2021010103"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching PerProjectResponse
		// so unmarshal it to create an object of type PerProjectResponse.  Error-out if that fails.
		obj := entities.PerProjectResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.Len(t, obj.Items, 4, "Invalid number of results")
	})

	// Request with an invalid granularity (should fail with return status code 400)
	t.Run("yearly granularity is invalid for this endpoint", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/analytics.wikimedia.org/all-access/all-agents/yearly/20190101/20190102"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 400 (http.StatusBadRequest).
		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// Since this request returns an error, it should return a JSON object of a different type
		// (one conforming to RFC 7807).  Unmarshal this to an httperr struct
		obj := httperr{}

		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		//Validating that the resulting obj.Detail attribute is non-zero in length should be Good Enough™ for now.
		assert.NotEmpty(t, obj.Detail)
		assert.NotEmpty(t, obj.Title)
	})

	// Request with an invalid agent (should fail with return status code 400)
	t.Run("app is not a valid agent for this endpoint", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/analytics.wikimedia.org/all-access/app/monthly/20190101/20190102"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 400 (http.StatusBadRequest).
		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// Since this request returns an error, it should return a JSON object of a different type
		// (one conforming to RFC 7807).  Unmarshal this to an httperr struct
		obj := httperr{}

		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		//Validating that the resulting obj.Detail attribute is non-zero in length should be Good Enough™ for now.
		assert.NotEmpty(t, obj.Detail)
		assert.NotEmpty(t, obj.Title)
	})

	// Request with an invalid access (should fail with return status code 400)
	t.Run("web is not a valid project for this endpoint", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/analytics.wikimedia.org/web/all-agents/monthly/20190101/20190102"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 400 (http.StatusBadRequest).
		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// Since this request returns an error, it should return a JSON object of a different type
		// (one conforming to RFC 7807).  Unmarshal this to an httperr struct
		obj := httperr{}

		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		//Validating that the resulting obj.Detail attribute is non-zero in length should be Good Enough™ for now.
		assert.NotEmpty(t, obj.Detail)
	})

	// Request with an invalid start timestamp (should fail with return status code 400)
	t.Run("invalid start timestamp", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/analytics.wikimedia.org/all-access/all-agents/monthly/201901a2/20190102"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 400 (http.StatusBadRequest).
		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// Since this request returns an error, it should return a JSON object of a different type
		// (one conforming to RFC 7807).  Unmarshal this to an httperr struct
		obj := httperr{}

		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		//Validating that the resulting obj.Detail attribute is non-zero in length should be Good Enough™ for now.
		assert.NotEmpty(t, obj.Detail)
	})

	// Request with an invalid end timestamp (should fail with return status code 400)
	t.Run("invalid end timestamp", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/analytics.wikimedia.org/all-access/all-agents/monthly/20190101/201901a2"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 400 (http.StatusBadRequest).
		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// Since this request returns an error, it should return a JSON object of a different type
		// (one conforming to RFC 7807).  Unmarshal this to an httperr struct
		obj := httperr{}

		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		//Validating that the resulting obj.Detail attribute is non-zero in length should be Good Enough™ for now.
		assert.NotEmpty(t, obj.Detail)
	})

	t.Run("returns 400 when start timestamp > end timestamp", func(t *testing.T) {
		// Perform an HTTP request, and error-out if it does not succeed.
		res, err := http.Get(apiTestURL("aggregate/en.wikipedia/all-access/all-agents/hourly/2021010103/2021010100"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 400 (http.StatusBadRequest).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching PerProjectResponse
		// so unmarshal it to create an object of type PerProjectResponse.  Error-out if that fails.
		obj := httperr{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validate the number of expected results for this request.
		assert.NotEmpty(t, obj.Detail)
	})

}
