package entities

// PerProject represents one result from the per-project resultset.
type PerProject struct {
	Project     string `json:"project"`
	Access      string `json:"access"`
	Agent       string `json:"agent"`
	Granularity string `json:"granularity"`
	Timestamp   string `json:"timestamp"`
	Views       int    `json:"views"`
}

// PerProjectResponse represents a container for the per-project resultset.
type PerProjectResponse struct {
	Items []PerProject `json:"items"`
}
