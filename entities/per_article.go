package entities

// PerArticle represents an entry in the API resultset.
type PerArticle struct {
	Project     string `json:"project" example:"en.wikipedia.org"` // Wikimedia project domain
	Article     string `json:"article" example:"Jupiter"`          // Page title with spaces replaced with underscores
	Granularity string `json:"granularity" example:"monthly"`      // Frequency of data
	Timestamp   string `json:"timestamp" example:"2021020100"`     // Timestamp in YYYYMMDDHH format
	Access      string `json:"access" example:"all-access"`        // Access method
	Agent       string `json:"agent" example:"user"`               // Agent type
	Views       int    `json:"views" example:"11609"`              // Number of pageviews
}

// PerArticleResponse represents the API resultset	.
type PerArticleResponse struct {
	Items []PerArticle `json:"items"`
}
