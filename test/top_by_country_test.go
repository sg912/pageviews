package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"testing"

	"pageviews/entities"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTopByCountry(t *testing.T) {
	// Valid request (should pass with return status code 200)
	t.Run("ceiled values should be converted to intervals when request is valid", func(t *testing.T) {

		res, err := http.Get(apiTestURL("top-by-country/en.wikipedia/all-access/2020/02"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails).
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching TopByCountryResponse
		// so unmarshal it to create an object of type TopByCountryResponse.  Error-out if that fails.
		obj := entities.TopByCountryResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		assert.Equal(t, "1000000000-9999999999", obj.Items[0].Countries[0].Views)

	})

	//Valid request(should pass with return status code 200)
	t.Run("returns 200 for valid request", func(t *testing.T) {

		res, err := http.Get(apiTestURL("top-by-country/en.wikipedia/all-access/2020/02"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 200 (http.StatusOK).  We error-out if it is otherwise,
		// because the validations below cannot succeed.
		require.Equal(t, http.StatusOK, res.StatusCode, "Incorrect HTTP status code")

		// Read the response body, (and error-out if that fails)
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// The response body should be a JSON-encoded object with a schema matching TopByCountryResponse
		// so unmarshal it to create an object of type TopByCountryResponse.  Error-out if that fails.
		obj := entities.TopByCountryResponse{}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		//Validate the number of expected results for this request
		assert.Len(t, obj.Items, 1, "Invalid number of results")

	})

	// Request with an invalid access (should fail with return status code 500)
	t.Run("invalid access", func(t *testing.T) {

		res, err := http.Get(apiTestURL("top-by-country/en.wikipedia/invalid-access/2020/02"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 500 (http.StatusInternalServerError).
		require.Equal(t, http.StatusInternalServerError, res.StatusCode)

		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// Since this request returns an error, it should return a JSON object of a different type
		// (one conforming to RFC 7807).  Unmarshal this to an httperr struct (see top of file), similar
		obj := httperr{
			Detail: "",
		}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validating that the resulting obj.Detail attribute is non-zero in length should be Good Enough™ for now.
		assert.NotEmpty(t, obj.Detail)

	})

	// Request with an invalid day (should fail with return status code 500)
	t.Run("invalid day", func(t *testing.T) {

		res, err := http.Get(apiTestURL("top-by-country/en.wikipedia/invalid-access/2020/32"))
		require.NoError(t, err, "HTTP request error")

		// Validate a return status of 500 (http.StatusInternalServerError).
		require.Equal(t, http.StatusInternalServerError, res.StatusCode)

		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response body")

		// Since this request returns an error, it should return a JSON object of a different type
		// (one conforming to RFC 7807).  Unmarshal this to an httperr struct (see top of file), similar
		obj := httperr{
			Detail: "",
		}
		err = json.Unmarshal(body, &obj)
		require.NoError(t, err, "Unmarshalling response object")

		// Validating that the resulting obj.Detail attribute is non-zero in length should be Good Enough™ for now.
		assert.NotEmpty(t, obj.Detail)

	})
}
