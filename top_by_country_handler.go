package main

import (
	"context"
	"encoding/json"
	"net/http"
	"pageviews/logic"
	"strings"
	"time"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// TopByCountryHandler is the HTTP handler for top-by-country API requests.
type TopByCountryHandler struct {
	logger  *logger.Logger
	session *gocql.Session
	logic   *logic.TopByCountryLogic
	config  *Config
}

func (s *TopByCountryHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	var err error
	project := strings.TrimSuffix(strings.ToLower(ctx.UserValue("project").(string)), ".org")
	access := strings.ToLower(ctx.UserValue("access").(string))
	year := ctx.UserValue("year").(string)
	month := ctx.UserValue("month").(string)

	c, cancel := context.WithTimeout(ctx, time.Duration(s.config.ContextTimeout)*time.Millisecond)

	response := s.logic.ProcessTopByCountryLogic(c, ctx, project, access, year, month, ctx.Request.URI().String(), s.logger, s.session)
	defer cancel()

	if response == nil {
		return
	}

	ctx.SetStatusCode(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Log(logger.ERROR, "Unable to marshal response object: %s", err)

		statusCode := http.StatusInternalServerError
		problemResp := aqsassist.CreateProblem(statusCode, err.Error(), string(ctx.Request.URI().RequestURI())).JSON()

		ctx.SetStatusCode(statusCode)
		ctx.SetBody(problemResp)
		return
	}
	ctx.SetBody(data)
}
