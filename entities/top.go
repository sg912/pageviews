package entities

// Top represents an entry in the API resultset.
type Top struct {
	Project  string    `json:"project"`
	Access   string    `json:"access"`
	Year     string    `json:"year"`
	Month    string    `json:"month"`
	Day      string    `json:"day"`
	Articles []Article `json:"articles"`
}

// Article represents article data in a Top result.
type Article struct {
	Article string `json:"article"`
	Views   int    `json:"views"`
	Rank    int    `json:"rank"`
}

// TopResponse represents the API resultset.
type TopResponse struct {
	Items []Top `json:"items"`
}
