package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"pageviews/logic"
	"strings"
	"time"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// PerArticleHandler is an HTTP handler for per-article API requests.
type PerArticleHandler struct {
	logger  *logger.Logger
	session *gocql.Session
	logic   *logic.PerArticleLogic
	config  *Config
}

// API documentation
// @summary      Get pageviews for a page.
// @router       /per-article/{project}/{access}/{agent}/{article}/{granularity}/{start}/{end} [get]
// @description  Given a wiki page and a date range, returns a time series of pageview counts.
// @param        project      path  string  true  "Domain of a Wikimedia project"                                                example(en.wikipedia.org)
// @param        access       path  string  true  "Method of access" Enums(all-access, desktop, mobile-app, mobile-web)          example(all-access)
// @param        agent        path  string  true  "Type of agent" Enums(all-agents, user, spider, automated)                     example(user)
// @param        article      path  string  true  "Page title in URL-encoded format. The sandbox encodes titles automatically."  example(Jupiter)
// @param        granularity  path  string  true  "Frequency of response data" Enums(daily, monthly)                             example(monthly)
// @param        start        path  string  true  "Date of the first day to include, in YYYYMMDD or YYYYMMDDHH format"           example(20210101)
// @param        end          path  string  true  "Date of the last day to include, in YYYYMMDD or YYYYMMDDHH format"            example(20210701)
// @produce      json
// @success      200 {object} PerArticleResponse
func (s *PerArticleHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	var err error

	// Parameters
	var access = strings.ToLower(ctx.UserValue("access").(string))
	var agent = strings.ToLower(ctx.UserValue("agent").(string))
	var article, _ = url.QueryUnescape(ctx.UserValue("article").(string))
	var granularity = strings.ToLower(ctx.UserValue("granularity").(string))
	var project = strings.TrimSuffix(strings.ToLower(ctx.UserValue("project").(string)), ".org")
	var start, end string

	aggregate, err := aggregateAttribute(access, agent)
	if err != nil {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    nil,
		})
		return
	}

	// Parameter validation
	if granularity != "daily" && granularity != "monthly" {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Invalid granularity",
			URL:    nil,
		})
		return
	}

	if start, err = aqsassist.ValidateTimestamp(ctx.UserValue("start").(string)); err != nil {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Invalid start timestamp",
			URL:    nil,
		})

		return
	}
	if end, err = aqsassist.ValidateTimestamp(ctx.UserValue("end").(string)); err != nil {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Invalid end timestamp",
			URL:    nil,
		})
		return
	}

	if err = aqsassist.StartBeforeEnd(start, end); err != nil {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "start timestamp should be before the end timestamp",
			URL:    nil,
		})

		return
	}

	if _, _, err = aqsassist.ValidateDuration(ctx.UserValue("start").(string), ctx.UserValue("end").(string), granularity); err != nil {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "no full months found in specified date range",
			URL:    nil,
		})
		return
	}

	c, cancel := context.WithTimeout(ctx, time.Duration(s.config.ContextTimeout)*time.Millisecond)
	response := s.logic.ProcessPerArticleLogic(c, ctx, access, agent, project, article, granularity, start, end, ctx.Request.URI().String(), aggregate, s.logger, s.session)
	defer cancel()

	if response == nil {
		return
	}

	ctx.SetStatusCode(http.StatusOK)

	var data []byte

	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Log(logger.ERROR, "Unable to marshal response object: %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    nil,
		})

		return
	}

	ctx.SetBody(data)
}

// FIXME: This probably needs a more sensible name...
func aggregateAttribute(access, agent string) (string, error) {
	switch access {
	case "all-access":
		switch agent {
		case "all-agents":
			return "aa", nil
		case "automated":
			return "ab", nil
		case "spider":
			return "as", nil
		case "user":
			return "au", nil
		default:
			return "", fmt.Errorf("Invalid agent string")
		}
	case "desktop":
		switch agent {
		case "all-agents":
			return "da", nil
		case "automated":
			return "db", nil
		case "spider":
			return "ds", nil
		case "user":
			return "du", nil
		default:
			return "", fmt.Errorf("Invalid agent string")
		}
	case "mobile-app":
		switch agent {
		case "all-agents":
			return "maa", nil
		case "automated":
			return "mab", nil
		case "spider":
			return "mas", nil
		case "user":
			return "mau", nil
		default:
			return "", fmt.Errorf("Invalid agent string")
		}
	case "mobile-web":
		switch agent {
		case "all-agents":
			return "mwa", nil
		case "automated":
			return "mwb", nil
		case "spider":
			return "mws", nil
		case "user":
			return "mwu", nil
		default:
			return "", fmt.Errorf("Invalid agent string")
		}
	default:
		return "", fmt.Errorf("Invalid access string")
	}
}
