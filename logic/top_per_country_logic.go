package logic

import (
	"context"
	"encoding/json"
	"pageviews/entities"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

type TopPerCountryLogic struct {
}

func (s *TopPerCountryLogic) ProcessTopPerCountryLogic(context context.Context, ctx *fasthttp.RequestCtx, country, access, year, month, day, requestURI string, requestLogger *logger.Logger, session *gocql.Session) *entities.TopPerCountryResponse {
	var err error
	var articleSet = []entities.PerCountryArticle{}
	var response = entities.TopPerCountryResponse{Items: make([]entities.TopPerCountry, 0)}
	var articlesData string

	query := `SELECT "articles" FROM "local_group_default_T_top_percountry".data WHERE "_domain" = 'analytics.wikimedia.org' AND country = ? AND access = ? AND year = ? AND month = ? AND day = ?`
	if err := session.Query(query, country, access, year, month, day).WithContext(context).Consistency(gocql.One).Scan(&articlesData); err != nil {
		requestLogger.Log(logger.ERROR, "Query failed: %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    &requestURI,
		})
		return nil
	}

	if err = json.Unmarshal([]byte(articlesData), &articleSet); err != nil {
		requestLogger.Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    &requestURI,
		})
		return nil
	}

	response.Items = append(response.Items, entities.TopPerCountry{
		Country:  country,
		Access:   access,
		Year:     year,
		Month:    month,
		Day:      day,
		Articles: articleSet,
	})

	return &response
}
