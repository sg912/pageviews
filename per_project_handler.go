package main

import (
	"context"
	"encoding/json"
	"net/http"
	"pageviews/logic"
	"strings"
	"time"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// PerProjectHandler is the HTTP handler for per-project API requests.
type PerProjectHandler struct {
	logger  *logger.Logger
	session *gocql.Session
	logic   *logic.PerProjectLogic
	config  *Config
}

func (s *PerProjectHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	var err error

	project := aqsassist.TrimProjectDomain(ctx.UserValue("project").(string))
	access := strings.ToLower(ctx.UserValue("access").(string))
	agent := strings.ToLower(ctx.UserValue("agent").(string))
	granularity := strings.ToLower(ctx.UserValue("granularity").(string))
	var start, end string
	if granularity != "daily" && granularity != "monthly" && granularity != "hourly" {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Invalid granularity",
			URL:    nil,
		})
		return
	}

	if agent != "all-agents" && agent != "automated" && agent != "spider" && agent != "user" {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Invalid agent string",
			URL:    nil,
		})
		return
	}

	if access != "all-access" && access != "desktop" && access != "mobile-app" && access != "mobile-web" {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Invalid access string",
			URL:    nil,
		})
		return
	}

	if start, err = aqsassist.ValidateTimestamp(ctx.UserValue("start").(string)); err != nil {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Invalid timestamp",
			URL:    nil,
		})
		return
	}
	if end, err = aqsassist.ValidateTimestamp(ctx.UserValue("end").(string)); err != nil {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Invalid timestamp",
			URL:    nil,
		})
		return
	}

	if err = aqsassist.StartBeforeEnd(start, end); err != nil {
		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "start timestamp should be before the end timestamp",
			URL:    nil,
		})
		return
	}

	if start, end, err = aqsassist.ValidateDuration(start, end, granularity); err != nil {

		aqsassist.BadRequestValidationWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: "Invalid timestamp",
			URL:    nil,
		})
		return
	}

	c, cancel := context.WithTimeout(ctx, time.Duration(s.config.ContextTimeout)*time.Millisecond)
	response := s.logic.ProcessPerProjectLogic(c, ctx, project, access, agent, granularity, start, end, ctx.Request.URI().String(), s.logger, s.session)
	defer cancel()

	if response == nil {
		return
	}

	ctx.SetStatusCode(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Log(logger.ERROR, "Unable to marshal response object: %s", err)
		aqsassist.InternalServerErrorWrapper(aqsassist.ValidationWrapper{
			Ctx:    ctx,
			Detail: err.Error(),
			URL:    nil,
		})
		return
	}
	ctx.SetBody(data)
}
